from flask import Flask, render_template
import json
app = Flask(__name__)

#if url has no additional content, display index page
@app.route('/')
def index():
  return render_template('index.html', title='Home')

#if url contains /sweet, display sweet snacks general info page
@app.route('/sweet')
def sweet():
  return render_template('sweet.html', title='Sweet')

 #if url contains /sweet/chocolate, display chocolate detail page
@app.route('/sweet/chocolate')
def chocolate():
  return render_template('chocolate.html', title='Chocolates')

#if url contains /sweet/biscuit, display biscuit detail page
@app.route('/sweet/biscuit')
def biscuit():
  return render_template('biscuit.html', title='Biscuits')

#if url contains /sweet/candy, display candy detail page
@app.route('/sweet/candy')
def candy():
  return render_template('candy.html', title='Candy')

#if url contains /savoury, display savoury snack general info page
@app.route('/savory')
def savory():
  return render_template('savory.html', title='Savory')
  
#if url contains /healthy, display healthy snack general info page
@app.route('/healthy')
def healthy():
  return render_template('healthy.html', title='Healthy')

#if url contains /savoury/walkers, load the snacks JSON file and display the walkers details page
@app.route('/savory/walkers')
def walkers():
  snacks = []
  with open('snacks.json','r') as f:
     snacks = json.load(f)
     f.close()

#loop through each item in the JSON file
#if the item has 'walkers' in the brand field
#print the item to allow access on the HTML page
  p = {}
  for item in snacks:
     if item['Brand'] == "Walkers":
       print item
       p = item
	   
  return render_template('walkers.html', walkers=p, title='Walkers')
  
#if url contains /savoury/pringles, load the snacks JSON file and display the pringles details page
@app.route('/savory/pringles')
def pringles():
  snacks = []
  with open('snacks.json','r') as f:
     snacks = json.load(f)
     f.close()
#loop through each item in the JSON file
#if the item has 'pringles' in the brand field
#print the item to allow access on the HTML page
  p = {}
  for item in snacks:
     if item['Brand'] == "Pringles":
       print item
       p = item
	   
  return render_template('pringles.html', pringles=p, title='Pringles')
 
#if url contains /savoury/mccoys, load the snacks JSON file and display the mccoys details page
@app.route('/savory/mccoys')
def mccoys():
  snacks = []
  with open('snacks.json','r') as f:
     snacks = json.load(f)
     f.close()

#loop through each item in the JSON file
#if the item has 'mccoys' in the brand field
#print the item to allow access on the HTML page
  p = {}
  for item in snacks:
     if item['Brand'] == "McCoys":
       print item
       p = item
	   
  return render_template('mccoys.html', mccoys=p, title='McCoys')

#if url contains /savoury/walkers/walkersnutrition, display the walkers crisps nutrition page
@app.route('/savory/walkers/walkersnutrition')
def walkersnutrition():
  return render_template('walkersnutrition.html', title='Walkers-Nutrition')
  
#if url contains /savoury/pringles/pringlesnutrition, display the pringles crisps nutrition page
@app.route('/savory/pringles/pringlesnutrition')
def pringlesnutrition():
  return render_template('pringlesnutrition.html', title='Pringles-Nutrition')
 
#if url contains /savoury/mccoys/mccoysnutrition, display the mccoys crisps nutrition page
@app.route('/savory/mccoys/mccoysnutrition')
def mccoysnutrition():
  return render_template('mccoysnutrition.html', title='McCoys-Nutrition')


if __name__ == "__main__":
  app.run(host='0.0.0.0', debug=True)
